//***************************************************************************************************************************************************************
//
// Firmware fuer Thermoflow Sonde		21.10.2009		
// gemeinsamer Kommunikationsteil fuer Master und Slave
// Listing: Rechter Rand 160 Zeichen 10 Punkt Courier
//
//***************************************************************************************************************************************************************

//#define USE_STDPERIPH_DRIVER	// Anweisung an CMSIS framework alle anderen erforderlichen Header einzuschliessen
//#define STM32F10X_CL			// bedingte Compilierung fuer Conectivity Line Devices, ebenso im Linkerfile link.ld und zum Flashen im Projekt anzupassen

#include "stm32f10x.h"			// Includes von STM
#include "com.h"
#include "globals.h"

/**************************************************************************************************************************************************************/
//**
//** einen moeglicherweise uebergelaufenen Uart1 Empfaenger inklusiv Empfangspuffer zuruecksetzen und wieder einschalten

void ResetRx1 (void)
{
 //volatile int8_t temp;
 (void) USART1->SR;								// SR-DR Lesesequenz setzt alle Bits des Empfaengers zurueck
 (void) USART1->DR;								// das sind RXNE, IDLE, ORE Overrun Error, NE Noise Error, FE Frame Error, PE Parity Error
 Rx1WrPtr = 0;									// Erklaere Empfangspuffer fuer ungueltig
 Rx1RdPtr = 0;									// Lesezeiger auf Anfang stellen
USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);	// Maske fuer Empfangsinterrupt aufschalten
}

