/*
  *******************************************************************************
  * @Project  Firmware for Splitter 
  * @author   Veith Mikroelektronik GmbH
  * @version  V_02
  * @date     05-May-2021
  * @brief    
  ******************************************************************************
*/ 
/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"			// Includes von STM
#include "splitter.h"		        // Includes Private
/* Global variables ----------------------------------------------------------*/           
uint16_t revtime;       // variable to store revolution time (diff between two interrupts) 
uint16_t oldcount;      // variable to store old count
int8_t Sp_Zust = 0;     // state variable for speed
int8_t dr_Zust = 0;     // state variable to detect rotation speeds below 100 rpm
int16_t	dr_Timer;       // timer variable to detect rotation speeds below 100 rpm
uint16_t actpos;        // var to store actual pos for angle cal
uint16_t oldpos;        // var to store old pos for angle cal
uint16_t resol;         // resolution of incremental ctr
/**************************************************************************/
//**
//**  Angle calculation
void angle_cal(void)	                     
{
 uint16_t refresol = resol;  // local var to store resol           
 if (resol == 4096)          // resol-4096, Anz-1024 
 {DAC_SetChannel1Data(DAC_Align_12b_R, (TIM_GetCounter(TIM4)));     // Set DAC CH_1 DHR12R register (12 bit output value in right aligned buffer register)
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                    // Start DAC Channel1 conversion by software 
  if (refresol != resol) TIM_SetCounter(TIM4,0);                    // reset TIM4 ctr if resol is changed
  TIM4->ARR = 65535;                                                // ARR val for bin resol 2^x
 } 
 if (resol == 8192)    //resol-8192, Anz-2048
 {DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4))/2));     // Set DAC CH_1 DHR12R register (12 bit output value in right aligned buffer register)
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                 	 // Start DAC CH_1 conversion by software 
  if (refresol != resol)  TIM_SetCounter(TIM4,0);                       // reset TIM4 ctr if resol is changed 
  TIM4->ARR = 65535;  
 }
 if (resol == 16384)  //resol-16384, Anz-4096
 {DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4))/4));     
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                	
  if (refresol != resol)  TIM_SetCounter(TIM4,0);
  TIM4->ARR = 65535;  
 }
 if (resol == 2048)  //resol-2045, Anz-512
 {DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4)) * 2));    
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                         
  if (refresol != resol)  TIM_SetCounter(TIM4,0);
  TIM4->ARR = 65535; 
 }
 if (resol == 1024)  //resol-1024, Anz-256
 {DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4)) * 4));    
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                          
  if (refresol != resol) TIM_SetCounter(TIM4,0);
  TIM4->ARR = 65535; 
 }  
 if (resol == 4000)  //resol-4000, Anz-1000
 {DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4))));    
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                          
  if (refresol != resol) TIM_SetCounter(TIM4,0);
  TIM4->ARR = 7999;                                // ARR val for dec resol 10^x
 }  
 if (resol == 2000)  //resol-2000, Anz-500
 {DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4)) * 2));    
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                          
  if (refresol != resol) TIM_SetCounter(TIM4,0);
  TIM4->ARR = 3999; 
 } 
 if (resol == 1000)  //resol-1000, Anz-250
 {DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4)) * 4));    
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                          
  if (refresol != resol) TIM_SetCounter(TIM4,0);
  TIM4->ARR = 3999;                               
 }
 if (resol == 800)  //resol-800, Anz-200
 {DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4)) * 5));    
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                          
  if (refresol != resol) TIM_SetCounter(TIM4,0);
  TIM4->ARR = 3999;
 }
 if (resol == 500)  //resol-500, Anz-125
 {DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4)) * 8));    
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                          
  if (refresol != resol) TIM_SetCounter(TIM4,0);
  TIM4->ARR = 3999;
 }     
 if (resol == 400)  //resol-400, Anz-100
 { DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4)) * 10));    
   DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                          
   if (refresol != resol) TIM_SetCounter(TIM4,0);
   TIM4->ARR = 3999;
 }       
 if (resol == 200)  //resol-200, Anz-50
 { DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4)) * 20));    
   DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                          
   if (refresol != resol) TIM_SetCounter(TIM4,0);
   TIM4->ARR = 3999;                              
 }    
 if (resol == 100)  //resol-100, Anz-25
 { DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4)) * 40));    
   DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                          
   if (refresol != resol) TIM_SetCounter(TIM4,0);
   TIM4->ARR = 3999;                              
 } 
/* 
 if (resol == 0)  //resol-0, Anz-25
 {DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4))));    
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                          
  if (refresol != resol) TIM_SetCounter(TIM4,0);
  TIM4->ARR = 3999;                              
 }      
*/
 if (resol == 0)  //resol-0, Anz-25
 {
  DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4))/2));    
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                          
  if (refresol != resol) TIM_SetCounter(TIM4,0);
  TIM4->ARR = 7999;                              
 }
 if (resol == 96)  //resol-0, Anz-25
 {DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4))));    
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                          
  if (refresol != resol) TIM_SetCounter(TIM4,0);
  TIM4->ARR = 65535;                                
 }  
 if (resol == 16000)  //resol-16000, Anz-4000
 {DAC_SetChannel1Data(DAC_Align_12b_R, ((TIM_GetCounter(TIM4))/4));     
  DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                	
  if (refresol != resol)  TIM_SetCounter(TIM4,0);
  TIM4->ARR = 3999;  
 }              
}
/**************************************************************************/
//**
//** ISR Speed Measurement State 0 - Inititializing
void Sp_Z0(void)
{    
  oldcount =  tictac;                   // store first system time of interrupt  			
  TIM_SetCounter(TIM4,0);		// Rest the angle incremental counter           
  Sp_Zust = 1;                          // Measure speed
}
//** ISR Speed Measurement State 1 - Calculating Speed
void Sp_Z1(void)
{
  uint16_t DACsp_val;                  // local var to dac speed val                                 
  actpos = TIM_GetCounter(TIM4);       // get the actual pos for angle cal
  resol = oldpos - actpos ;            // resol (CPR) 
  if (resol > 32768) resol = actpos - oldpos ; // opposite turning direction
  oldpos = actpos;  
  ISR_Flags |= rotactive;            // set rotactive flag 
  revtime = tictac - oldcount;       // time for one revolution is the diffrence to previous interrupts system time
  oldcount = tictac;                 // actual time becomes previous time, 1 tic = 0,1msec  
  DACsp_val= 491400/revtime;	     // (819 * 600 / revtime )
  if (DACsp_val > 4095)  DACsp_val = 4095;  // upper limit of speed max speed is 50000 rpm. 
  DAC_SetChannel2Data(DAC_Align_12b_R, (int16_t) (DACsp_val));         // Set DAC Channel2 DHR12R register (12 bit output value in right aligned buffer register)
  DAC_SoftwareTriggerCmd(DAC_Channel_2, ENABLE);                       // Start DAC Channel2 conversion by software 
}
//** ISR Speed Measurement State 2 - Re-Inititializing speeds below 100 rpm
void Sp_Z2(void)
{    
  oldcount =  tictac;                   // store first system time of interrupt 			
  ISR_Flags |= rotactive;               // set rotactive flag   
  TIM_SetCounter(TIM4,0);		// Rest the angle incremental counter              
}
//** Interrupt Service for Encoder Index Pulse for Speed Measurement
void EXTI9_5_IRQHandler(void)
{ switch (Sp_Zust)				
  { case 0: Sp_Z0(); break;      // Init		
    case 1: Sp_Z1(); break;      // Measure
    case 2: Sp_Z2(); break;      // Re-Init
  }
 EXTI_ClearITPendingBit(EXTI_Line5);
}

/**************************************************************************************************************************************************************/
//**
//** DAC_low (Speed< 80 rpm) and DAC_High (Speed > 100 rpm) init
void DAC_low(void) 
{
  GPIO_InitTypeDef 		GPIO_InitStructure;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;                  // PA5 for angle calculation 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;           // PA5 pin configured as digital out 
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;             
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  DAC_Cmd(DAC_Channel_2, DISABLE);                           // DISABLE DAC Channel2 for low speed
}
void DACh_high(void)
{
  GPIO_InitTypeDef 		GPIO_InitStructure;
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_5;               // PA5 for Speed and PA4 for angle calculation 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;            // In order to avoid parasitic consumption,the GPIO pin should be configured to analog(AIN) 
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  DAC_Cmd(DAC_Channel_2, ENABLE);                          // Enable DAC Channel2(for high speed): Once the DAC channel2 is enabled, PA.05 is automatically connected to the DAC converter.
}
/**************************************************************************************************************************************************************/
//**
//** Detect Rotation speed below 100 rpm
void dr_Z0(void)	                     // Z0: Init
{ISR_Flags &=~rotactive;		     // Reset rotactive flag 
 SetFlop (&dr_Timer,7500);                   // 750 ms (80rpm)
 dr_Zust = 1;                                    
}
void dr_Z1(void)                            // Z1: Normal rotation
{if (TestFlop(&dr_Timer))		    // when it is time for the next activity check 
 {SetFlop (&dr_Timer,7500);		    // check again in a 750 ms (<80 rpm)
  if (ISR_Flags & rotactive)                // if rotation was active
      ISR_Flags &=~rotactive;               // reset the activity flag
  else
  { dr_Zust = 2;			    // go to state Z2 (i.e. Wait for 1st index state) 			
    Sp_Zust = 2;                            // Coupling (Re-Init)
}}}
void dr_Z2(void)                            // Z2: Wait for 1st index
{if (ISR_Flags & rotactive)                 // wait for 1st index pulse
  {SetFlop (&dr_Timer,6000);                // start new time
   ISR_Flags &=~rotactive;                  // reset the activity flag
   dr_Zust = 3;			            // go to state Z3 (i.e. Wait for 2st index state) 	
   DAC_low();                               // DAC_low init for speed < 80 rpm                     		
}} 
void dr_Z3(void)                            // Z3: Wait for 2nd index
{if (TestFlop(&dr_Timer))		    // when it is time for the next activity check 
 {if (ISR_Flags & rotactive)                // if index pulse is active
   { dr_Zust = 1;                           // go to state Z1 ( speed > 100 rpm)
     Sp_Zust = 1;                           // Coupling (Measure)
     DACh_high();                           // DAC_high init for speed > 100 rpm          
   }
  else 	 dr_Zust = 2;			    // go to state Z2 ( speed < 80 rpm)		
}}
void detect_speed(void)                 
{switch (dr_Zust)			// check state
 {case 0:	dr_Z0(); break;		// Init
  case 1:	dr_Z1(); break;		// Normal speed
  case 2:	dr_Z2(); break;	        // Low speed 1st index      
  case 3:	dr_Z3(); break;         // Low speed 2nd index
}}

/**************************************************************************************************************************************************************/
//**
//** Main function
int main(void)
{
 cpuinit();  // init processor peripherals
 while (1)
 { 
   detect_speed();    // detect rotation speeds below 100 rpm
   angle_cal();       // angle calculation
 }
}



