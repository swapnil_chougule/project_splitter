//**
//******************************************************************************
//**          peripherals interrupt service routine.
//******************************************************************************

#include "stm32f10x_it.h"
#include "stm32f10x_conf.h"
#include "globals.h"			// darf hier nicht ueber thermoflow.h eingebunden werden da sonst doppelte Reservierung
#include "flop.h"				// im ISR File muessen die von anderen Modulen benutzten Header jeweils einzeln angegeben werden
#include "com.h"
#include "stm32f10x_exti.h"
//********************************************************************************************************************************
//**
//** lokale Variablen

__IO uint16_t T3_CC_alt, T3_CC_neu;		// Frequenzmessung fuer Wasserzaehler
__IO uint16_t T2_CC_alt, T2_CC_neu;		// Frequenzmessung fuer Messrad

//********************************************************************************************************************************
//**
// leere Interrupt Service Funktion 

void NMI_Handler(void) {}
void HardFault_Handler(void) {while (1){}}
void MemManage_Handler(void) {while (1){}}
void BusFault_Handler(void)  {while (1){}}
void UsageFault_Handler(void){while (1){}}
void SVC_Handler(void) {}
void DebugMon_Handler(void){}
void PendSV_Handler(void){}

//*********************************************************************************************************************************
//**
//** Interrupt Service Funktion der Systemzeituhr

void SysTick_Handler(void)
{
tictac++;
}

//*********************************************************************************************************************************
//**
//** Interrupt Service Funktion, Wegmessung auf Messrad mit Erweiterung auf 32 bit
//** der 16 bit Zaehler zaehlt 1/2 mm pro Takt. Mit 16 bit sind deshalb nur 16,384 Meter moeglich 

 void TIM4_IRQHandler(void)
{if(TIM_GetITStatus(TIM4, TIM_IT_Update) == SET)	// wenn es ein T4 Uebertrag gewesen ist
 {TIM_ClearITPendingBit(TIM4, TIM_IT_Update);		// TIM4 Update Interrupt quittieren
  uint16_t snap = TIM4->CNT;				// Nachgucken wo der Zaehler aktuell steht
  if (snap < 0x7fff)	T4_32++;			// Uebertrag vorwaerts
  else			T4_32--;  			// Uebertrag rueckwaerts
  ISR_Flags &=~T4_Valid;				// Valid=0 meldet Resultat als unlaengst geaendert
}}





