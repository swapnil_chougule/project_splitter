/*
  *******************************************************************************
  * @Project  Firmware for Splitter 
  * @author   Vaith Microelectronics
  * @version  V_01
  * @date     09-April-2021
  * @brief    global variables
  ******************************************************************************
*/ 

#include <stdint.h>
#include <string.h>


#ifdef RESERVE			// globals.h wird in allen Sourcefiles eingebunden. Ueber RESERVE wird gesteuert, ob eine Variable tatsaechlich reserviert wird
  #define EXTERN		// oder ob deren Name nur zum Zugriff als EXTERN deklariert wird.
  #define INIT(x) = (x)
#else
  #define EXTERN extern
  #define INIT(x)
#endif

//**************************************************************************************************************************************************************
// 
//

EXTERN TIM_OCInitTypeDef	TIM_OCInitStructure;	// Timer Output Compare Initialisierung

//**************************************************************************************************************************************************************
// /*
EXTERN volatile uint16_t T3_Flow INIT (0);		// Wasseruhr Durchfluss Frequenzmessung von Timer 3 CH3
EXTERN volatile uint16_t TIM3_CH3_CC_State INIT (0);	// ISR Zustandsmerker der Wasseruhr Frequenzmessung
EXTERN volatile uint16_t T2_Speed INIT (0);		// Messrad Ist-Geschwindigkeit Frequenzmessung von Timer 2 CH3
EXTERN volatile uint16_t TIM2_CH3_CC_State INIT (0);	// ISR Zustandsmerker der Messrad Frequenzmessung
EXTERN volatile uint16_t T4_32 INIT (0);		// Verlaengerung von Timer 4 auf 32 bit

//********************************************************************************************************************************************************
//** 
//** Aktivitaetsflags zur Steuerung von Regler und Frequenzmessungen in den Interrupt Service Routinen

EXTERN uint16_t			ISR_Flags	INIT(0);
#define rotactive 		((uint16_t)0x0001)	        // Activity flag fo the rotation speeds below 100 rpm
#define EncAktiv 		((uint16_t)0x0002)		// Aktivitaetsflag des Messrad Encoders	

#define T4_Valid		((uint16_t)0x0008)		// Semaphore fuer Uebertrag der 32 bit Wegmessung
#define FluxAktiv 		((uint16_t)0x0010)		// Aktivitaetsflag des Wasserzaehlers
//********************************************************************************************************************************************************

#undef RESERVE
