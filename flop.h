extern volatile int16_t tictac;		// Systemzeituhr ist signed

//**************************************************************************************************************************************************************
//
// Universelle Zeitverwaltung mit Systick, TestFlop meldet eine abgelaufene Zeit mit TRUE zuruek

void SetFlop(int16_t* timer, int16_t length);
int TestFlop(int16_t* timer);


