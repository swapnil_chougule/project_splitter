#Makefile for Thermoflow (Winde) created on 10/03/2021 
#Test of Arm GCC and make under Ubuntu
#Parameters for the compiler 
# -Wall 		All warnings are enabled
# -O0                   Most optimizations are completely disabled at -O0, Reduce compilation time and make debugging produce the expected results
# -Os                   Optimize for size.  -Os enables all -O2 optimizations except those that often increase code size
# -xc                   input Language C, (can also be omitted because the compiler can recognize it from the file name)
# -funsigned-char       If not specified in the source text, the data type CHAR is assumed by the compiler as UNSIGNED byte 0-255, 
#                       signed-char would be -127 to + 128 and zero would be 0x80 and must be specified explicitly in the source text
# -mlittle-endian       Generate little-endian code.  This is the default when GCC is configured for an aarch64-*-* but not an aarch64_be-*-* target.
# -c		        Compile or assemble the source files, but do not link.  The linking stage simply is not done.  The ultimate output is in the form of an object file for each source file. 	
#                       By default, the object file name for a source file is made by replacing the suffix .c, .i, .s, etc., with .o. Unrecognized input files, not requiring compilation or assembly, are ignored.
# -mcpu=cortex-m3       Some Cortex-M3 cores can cause data corruption when "ldrd" instructions with overlapping destination and base registers are used. It avoids generating these instructions. 
# -mno-tpcs-frame       Generate a stack frame that is compliant with the Thumb Procedure Call Standard for all non-leaf functions.  (A leaf function is one that does not call any other functions.) 
# -mthumb               Use code for THUMB instruction set
# -mno-thumb-interwork  Generate code that supports calling between the ARM and Thumb instruction sets.  Without this option, on pre-v5 architectures, the two instruction sets cannot be reliably used inside one program.
# -gdwarf-version       Produce debugging information in DWARF format (if that is supported).  The value of version may be either 2, 3, 4 or 5; the default version for most targets is 4.
# -I./	                Include files are located in the current directory
# -o./object/file.o     Store object in target subdirectory ./object/
# --function-sections   The switch is passed on to the linker. Only for files from the STM library. A section is created for each function in the library. Only functions that are used then generate code
# rm -r Object/*        Delete all the files inside folder "Object" but keep the folder "Object"

./o/out.elf : ./o/main.o ./o/stm32f10x_rcc.o ./o/stm32f10x_flash.o  ./o/flop.o ./o/misc.o ./o/stm32f10x_tim.o ./o/stm32f10x_dac.o ./o/stm32f10x_gpio.o ./o/startup_stm32f10x_cl.o ./o/stm32f10x_exti.o ./o/cpuinit.o ./o/stm32f10x_it.o 
	arm-none-eabi-gcc -mcpu=cortex-m3 -Wl,-gc-sections -Wl,-M=map.txt -Wl,-T./link.ld ./o/main.o ./o/stm32f10x_rcc.o ./o/stm32f10x_flash.o ./o/cpuinit.o ./o/stm32f10x_it.o ./o/flop.o ./o/misc.o ./o/stm32f10x_tim.o ./o/stm32f10x_dac.o ./o/stm32f10x_gpio.o ./o/startup_stm32f10x_cl.o ./o/stm32f10x_exti.o -o./o/out.elf
./o/main.o: main.c
	arm-none-eabi-gcc -Wall -O0  -funsigned-char -xc -mlittle-endian -mthumb -mno-thumb-interwork -c -mcpu=cortex-m3 -mno-tpcs-frame -gdwarf-2 -oo/main.o main.c    
./o/stm32f10x_rcc.o: stm32f10x_rcc.c
	arm-none-eabi-gcc  -Wall -Os -funsigned-char -xc -mlittle-endian -mthumb -mno-thumb-interwork -c -mcpu=cortex-m3 -mno-tpcs-frame -gdwarf-2 --function-sections -oo/stm32f10x_rcc.o stm32f10x_rcc.c   
./o/stm32f10x_flash.o: stm32f10x_flash.c
	arm-none-eabi-gcc -Wall -Os -funsigned-char -xc -mlittle-endian -mthumb -mno-thumb-interwork -c -mcpu=cortex-m3 -mno-tpcs-frame -gdwarf-2 --function-sections -oo/stm32f10x_flash.o stm32f10x_flash.c
./o/cpuinit.o: cpuinit.c
	arm-none-eabi-gcc -Wall -Os -funsigned-char -xc -mlittle-endian -mthumb -mno-thumb-interwork -c -mcpu=cortex-m3 -mno-tpcs-frame -gdwarf-2 -save-temps -oo/cpuinit.o cpuinit.c
./o/stm32f10x_it.o: stm32f10x_it.c
	arm-none-eabi-gcc -Wall -Os -funsigned-char -xc -mlittle-endian -mthumb -mno-thumb-interwork -c -mcpu=cortex-m3 -mno-tpcs-frame -gdwarf-2 --function-sections -oo/stm32f10x_it.o stm32f10x_it.c
./o/flop.o: flop.c
	arm-none-eabi-gcc -Wall -Os -funsigned-char -xc -mlittle-endian -mthumb -mno-thumb-interwork -c -mcpu=cortex-m3 -mno-tpcs-frame -gdwarf-2 -oo/flop.o flop.c
./o/misc.o: misc.c
	arm-none-eabi-gcc -Wall -Os -funsigned-char -xc -mlittle-endian -mthumb -mno-thumb-interwork -c -mcpu=cortex-m3 -mno-tpcs-frame -gdwarf-2 --function-sections -oo/misc.o misc.c
./o/stm32f10x_tim.o: stm32f10x_tim.c
	arm-none-eabi-gcc -Wall -O0 -funsigned-char -xc -mlittle-endian -mthumb -mno-thumb-interwork -c -mcpu=cortex-m3 -mno-tpcs-frame -gdwarf-2 --function-sections -oo/stm32f10x_tim.o stm32f10x_tim.c
./o/stm32f10x_dac.o: stm32f10x_dac.c
	arm-none-eabi-gcc -Wall -Os -funsigned-char -xc -mlittle-endian -mthumb -mno-thumb-interwork -c -mcpu=cortex-m3 -mno-tpcs-frame -gdwarf-2 -oo/stm32f10x_dac.o stm32f10x_dac.c
./o/stm32f10x_gpio.o: stm32f10x_gpio.c
	arm-none-eabi-gcc -Wall -Os -funsigned-char -xc -mlittle-endian -mthumb -mno-thumb-interwork -c -mcpu=cortex-m3 -mno-tpcs-frame -gdwarf-2 --function-sections -oo/stm32f10x_gpio.o stm32f10x_gpio.c
./o/stm32f10x_exti.o: stm32f10x_exti.c
	arm-none-eabi-gcc -Wall -Os -funsigned-char -xc -mlittle-endian -mthumb -mno-thumb-interwork -c -mcpu=cortex-m3 -mno-tpcs-frame -gdwarf-2 --function-sections -oo/stm32f10x_exti.o stm32f10x_exti.c
./o/startup_stm32f10x_cl.o: startup_stm32f10x_cl.s
	arm-none-eabi-as -gdwarf2 -mcpu=cortex-m3 -mthumb -oo/startup_stm32f10x_cl.o startup_stm32f10x_cl.s
clean:
	rm -r o/* 




