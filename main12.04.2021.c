/*
  *******************************************************************************
  * @Project  Firmware for Splitter 
  * @author   Vaith Microelectronics
  * @version  V_01
  * @date     09-April-2021
  * @brief    
  ******************************************************************************
*/ 

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"			// Includes von STM
#include "splitter.h"		        // Includes Private


/* Function prototypes ------------------------------------------------------*/
void angle_cal(void);
void Sp_InitState(void);	
void Sp_MeasureState(void);
void SpeedCalc(void);
void drTest(void);
void DAC_low(void);
void DAC_high(void);

/* Global variables ----------------------------------------------------------*/
               
static int16_t revtime;           // variable to store revolution time (diff between two interrupts) 
static int16_t oldcount;          // variable to store old count
int8_t Sp_Zust = 0;		  // state variable for speed
int8_t dr_Zust =0 ;               // state variable to detect rotation speeds below 100 rpm
int16_t	dr_Timer;                 // timer variable to detect rotation speeds below 100 rpm

/* Main function ---------------------------------------------------------*/

int main(void)
{
 cpuinit();
 while (1)
 { 
   angle_cal();                   // angle cal
   drTest();                      // detect rotation speeds below 100 rpm
 }
}


/**************************************************************************/
//**
//**  Angle calculation

void angle_cal(void)
{
 DAC_SetChannel1Data(DAC_Align_12b_R, TIM_GetCounter(TIM4));     // Set DAC Channel1 DHR12R register (12 bit output value in right aligned buffer register)
 DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);                  // Start DAC Channel1 conversion by software 
}        

/**************************************************************************/
//**
//** ISR Speed Measurement State 0 - Inititializing
void Sp_Z0(void)
{    
  oldcount =  tictac;                   // store first system time of interrupt 			
  TIM_SetCounter(TIM4,0);		// Rest the angle incremental counter           
  Sp_Zust = 1;
}

//** ISR Speed Measurement State 1 - Calculating Speed
void Sp_Z1(void)
{
  uint16_t DAC_val;
  ISR_Flags |= rotactive;                                              // set rotactive flag 
  revtime = tictac - oldcount;                                         // time for one revolution is the diffrence to previous interrupts system time
  oldcount = tictac;
  DAC_val= 491400/revtime;			                       // actual time becomes previous time, 1 tic = 0,1msec
  if (DAC_val > 4095)  DAC_val = 4095;
  DAC_SetChannel2Data(DAC_Align_12b_R, (int16_t) (DAC_val));           // Set DAC Channel2 DHR12R register (12 bit output value in right aligned buffer register)
  DAC_SoftwareTriggerCmd(DAC_Channel_2, ENABLE);                       // Start DAC Channel2 conversion by software 
}

//** ISR Speed Measurement State 2 - Re-Inititializing speeds below 100 rpm
void Sp_Z2(void)
{    
  oldcount =  tictac;                   // store first system time of interrupt 			
  ISR_Flags |= rotactive;               // set rotactive flag        
}

//** Interrupt Service for Encoder Index Pulse for Speed Measurement
void EXTI9_5_IRQHandler(void)
{ switch (Sp_Zust)				
  { case 0: Sp_Z0(); break;      // Init		
    case 1: Sp_Z1(); break;      // Measure
    case 2: Sp_Z2(); break;      // Re-Init
  }
 EXTI_ClearITPendingBit(EXTI_Line5);
}

/**************************************************************************************************************************************************************/
//**
//** DAC_low (Speed< 80 rpm) and DAC_High (Speed > 100 rpm)

void DAC_low(void)
{
  GPIO_InitTypeDef 		GPIO_InitStructure;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;                         // PA4 for Speed and PA5 for angle calculation 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;                  //In order to avoid parasitic consumption,the GPIO pin should be configured to analog(AIN) 
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;             
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  DAC_Cmd(DAC_Channel_2, DISABLE);                                  // DISABLE DAC Channel2(Speed)
}

void DACh_high(void)
{
  GPIO_InitTypeDef 		GPIO_InitStructure;
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_5;                  // PA5 for Speed and PA4 for angle calculation 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;               //In order to avoid parasitic consumption,the GPIO pin should be configured to analog(AIN) 
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  DAC_Cmd(DAC_Channel_2, ENABLE); 
}

/**************************************************************************************************************************************************************/
//**
//** Detect Rotation speeds below 100 rpm

void dr_Z0(void)	                     // Z0: Init
{ISR_Flags &=~rotactive;		     // Reset rotactive flag 
 SetFlop (&dr_Timer,7500);                   // 750 ms (80rpm)
 dr_Zust = 1;                                    
}

void dr_Z1(void)                            // Z1: Normal rotation
{if (TestFlop(&dr_Timer))		    // when it is time for the next activity check 
 {SetFlop (&dr_Timer,7500);		    // check again in a 750 ms (<80 rpm)
  if (ISR_Flags & rotactive)                // if rotation was active
      ISR_Flags &=~rotactive;               // reset the activity flag
  else
  { dr_Zust = 2;			    // go to state Z2 (i.e. Wait for 1st index state) 			
    Sp_Zust = 2;                            // Coupling (Re-Init)
}}}

void dr_Z2(void)                            // Z2: Wait for 1st index
{if (ISR_Flags & rotactive)                 // wait for 1st index pulse
  {SetFlop (&dr_Timer,6000);                // start new time
   ISR_Flags &=~rotactive;                  // reset the activity flag
   dr_Zust = 3;			            // go to state Z3 (i.e. Wait for 2st index state) 	
   DAC_low();                               // DAC_low init for speed < 80 rpm                     		
}} 

void dr_Z3(void)                            // Z3: Wait for 2nd index
{if (TestFlop(&dr_Timer))		    // when it is time for the next activity check 
 {if (ISR_Flags & rotactive)                // if index pulse is active
   { dr_Zust = 1;                           // go to state Z1 ( speed > 100 rpm)
     Sp_Zust = 1;                           // Coupling (Measure)
     DACh_high();                           // DAC_high init for speed > 100 rpm          
   }
  else 	 dr_Zust = 2;			    // go to state Z2 ( speed < 80 rpm)		
}}

void drTest(void)
{switch (dr_Zust)			// check state
 {case 0:	dr_Z0(); break;		// Init
  case 1:	dr_Z1(); break;		// Normal speed
  case 2:	dr_Z2(); break;	        // Low speed 1st index      
  case 3:	dr_Z3(); break;         // Low speed 2nd index
}}

/**************************************************************************************************************************************************************/


