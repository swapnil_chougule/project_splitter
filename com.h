//***************************************************************************************************************************************************************
//
// Firmware fuer Thermoflow Sonde		21.10.2009		
// globale Variablen
// Listing: Rechter Rand 160 Zeichen 10 Punkt Courier
//
//***************************************************************************************************************************************************************

#include <stdint.h>				// Benutzt C99 Integer Typen

//***************************************************************************************************************************************************************
//**
//** Konstanten

#define CMD_LOAD_DAC_A	0x9000	// SPI Kommando zum DAC LTC 1661 Kanal A einstellen
#define CMD_LOAD_DAC_B	0xA000	// SPI Kommando zum DAC LTC 1661 Kanal B einstellen

#define SYNC	0xf0 			// Synchronisations Zeichen im Manchester Telegramm

//**************************************************************************************************************************************************************
//
// von com.c zur Verfuegug gestellte Funktionen

//int Putchar (char ch);
void ResetRx1(void);

