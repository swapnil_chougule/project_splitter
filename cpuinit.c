//***************************************************************************************************************************************************************
//**
//** Initialisierung des gesamten Prozessors und seiner Peripherie. 
//** HSE_Value gibt an, welcher Quartz angeschlossen ist. Der Wert steht in stm32f10x.h

#include "stm32f10x.h"					// einziges Include File, beinhaltet alle anderen Includes
#include "globals.h"
#include "stm32f10x_exti.h"
#define ADC1_DR_Address    ((uint32_t)0x4001244C)

//***************************************************************************************************************************************************************
//**
//** lokales RAM

GPIO_InitTypeDef 		GPIO_InitStructure;		// GPIO Initialisierung
NVIC_InitTypeDef 		NVIC_InitStructure;		// NVIC Initialisierung
TIM_TimeBaseInitTypeDef	        TIM_TimeBaseStructure;	        // Timer Vorteiler Initialisierung
TIM_ICInitTypeDef		TIM_ICInitStructure;	        // Timer Input Capture Initialsierung
DAC_InitTypeDef			DAC_InitStructure;		// DAC Initialisierung
EXTI_InitTypeDef                EXTI_InitStructure;             // External interrupt Initialisierung
/**************************************************************************************************************************************************************
//
// gesamte Peripherie initialisieren, zunaechst alle Einstellungen die mit der Takterzeugung zusammenhaengen  */

void cpuinit(void)
{ 
  RCC_DeInit();							// definierten Ausgangszustand herstellen
  RCC_HSEConfig(RCC_HSE_ON);					// Externer Quartzoszillator einschalten
  RCC_WaitForHSEStartUp();					// warte bis externer Quartz eingeschwungen ist

  FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);		// Flash Prefetch Buffer einschalten
  FLASH_SetLatency(FLASH_Latency_2);				// 2 Wartezyklen bei Flash Zugriffen

  RCC_HCLKConfig(RCC_SYSCLK_Div1);				// AHB Takt ist gleich Systemtakt 72 Mhz 
  RCC_PCLK2Config(RCC_HCLK_Div1);				// APB2 schnelle Peripherie hat auch 72 Mhz
  RCC_PCLK1Config(RCC_HCLK_Div2);				// APB1 langsame Peripherie kann max. 36 Mhz = 72/2
  RCC_ADCCLKConfig(RCC_PCLK2_Div6);				// ADC kann maximal 14 Mhz, daher 72/6=12 Mhz
  RCC_PREDIV1Config(RCC_PREDIV1_Source_HSE, RCC_PREDIV1_Div2);	// 16 Mhz / 2 = 8 MHz 
  RCC_PLLConfig(RCC_PLLSource_PREDIV1, RCC_PLLMul_9);		// 8 Mhz * 9 = 72 Mhz
  RCC_PLLCmd(ENABLE);						// PLL einschalten
  while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET){}		// warte bis PLL eingerastet ist
  RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);			// auf PLL als Taktquelle umschalten auswaehlen
  while(RCC_GetSYSCLKSource() != 0x08) {}			// erst weitermachen wenn Umschaltung auf PLL abgeschlossen ist
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);    // GPIOA, GPIOB and AFIO clock enable 
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 | RCC_APB1Periph_TIM4 | RCC_APB1Periph_DAC, ENABLE);       // TIM3, TIM4 and DAC clock enable    
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);		// DMA1 haengt am High Speed Bus, hierfuer Takt aufschalten
  SysTick_Config(7200);       // 100 us per tic


//**************************************************************************************************************************************************************
//
// Port A Push-Pull Outputs



//**************************************************************************************************************************************************************
//
// Port A Alternate Functions

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 ;        // TIM3_CH2 pin PA.07 one pulse output
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;   // Alternating fun PP o/p
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; // port speed 50MHz
  GPIO_Init(GPIOA, &GPIO_InitStructure);

//**************************************************************************************************************************************************************
//
// Port A analog outputs. The analog outputs must also be in AIN mode because of possible leakage currents)
  
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_4 | GPIO_Pin_5;        // PA5 for Speed and PA4 for angle calculation 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;                  //In order to avoid parasitic consumption,the GPIO pin should be configured to analog(AIN) 
  GPIO_Init(GPIOA, &GPIO_InitStructure);

//**************************************************************************************************************************************************************
//
// Port A digitale inputs

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;              // TIM3_CH1 pin (PA.06 : i/p) input for one pulse o/p
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;  // i/p config
  GPIO_Init(GPIOA, &GPIO_InitStructure);

//**************************************************************************************************************************************************************
//
// Port B Push-Pull	Outputs




//**************************************************************************************************************************************************************
//
// Port B digitale Eingaenge (Port B digitale Inputs)

  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6 |				       // PB6  ist Timer4 CH1 Quadratursignal A
								GPIO_Pin_7|    // PB7  ist Timer4 CH2 Quadratursignal B
             							GPIO_Pin_5|    // PB5 Extint (External impulse input signal) 
								GPIO_Pin_0;    // jumper for resol detection

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;                        // i/p config
  GPIO_Init(GPIOB, &GPIO_InitStructure);

//**************************************************************************************************************************************************************
//
// External interrupt Configuration

  GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource5);      // Connect EXTI05 Line to PB.05 pin 
  //Configure EXTI05 line (Impulse)
  EXTI_InitStructure.EXTI_Line = EXTI_Line5;          // External interrupt on              
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
  // Enable and set EXTI5 Interrupt to the lowest priority 
  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;                   // Enable and set EXTI9_5 Interrupt to the lowest priority
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);


//**************************************************************************************************************************************************************
//
// DAC Configuration
  DAC_InitStructure.DAC_Trigger = DAC_Trigger_Software;	              // DAC : Software trigger mode configuration				
  DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
  DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;  
  DAC_Init(DAC_Channel_1, &DAC_InitStructure);
  DAC_Init(DAC_Channel_2, &DAC_InitStructure);
  DAC_Cmd(DAC_Channel_1, ENABLE);                          // Enable DAC Channel1(Angle): Once the DAC channel1 is enabled, PA.04 is automatically connected to the DAC converter. 
  DAC_Cmd(DAC_Channel_2, ENABLE);                          // Enable DAC Channel2(Speed): Once the DAC channel1 is enabled, PA.05 is automatically connected to the DAC converter. 

//**************************************************************************************************************************************************************
//
// Timer 3 configuration 
// Timer 3 configuration (generate 1ms pulse after each revolutio, rising edge on PA.6 (T3CH1) generates 1ms pulse on PA.7 (T3CH2))
// one pulse = (ARR - CCR1) / TIM3_ctr_clk

  // Time base configuration 
  TIM_TimeBaseStructure.TIM_Period = 36050;                       // after OC2 fast enable ARR should be 36050 to get 1ms pulse
  TIM_TimeBaseStructure.TIM_Prescaler = 1;                        // Prescalar 1 (i.e TIM3_ctr_clk = 36 MHz)
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

  // TIM3 PWM2 Mode configuration: Channel2 
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = 29535;                            // capture value (CCR1)                   
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
  TIM_OC2Init(TIM3, &TIM_OCInitStructure);
  TIM_OC2FastConfig (TIM3, TIM_OCFast_Enable);                      // OC2 fast enable to minimize delay 

  // TIM3 configuration in Input Capture Mode 
  TIM_ICStructInit(&TIM_ICInitStructure);
  TIM_ICInitStructure.TIM_Channel = TIM_Channel_1;                  // T3CH1 PA.06 input pin 
  TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
  TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
  TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
  TIM_ICInitStructure.TIM_ICFilter = 0;
  TIM_ICInit(TIM3, &TIM_ICInitStructure);

  TIM_SelectOnePulseMode(TIM3, TIM_OPMode_Single);        // One Pulse Mode selection  
  TIM_SelectInputTrigger(TIM3, TIM_TS_TI1FP1);            // Input Trigger selection 
  TIM_SelectSlaveMode(TIM3, TIM_SlaveMode_Trigger);       // Slave Mode selection: Trigger Mode 

//**************************************************************************************************************************************************************
//
// Timer 4 counts the quadrature encoder on channel 1 and channel 2. 
// Timer 4 config in Encoder interface mode

TIM_TimeBaseStructure.TIM_Prescaler = 0;				 // Prescalar 0 (i.e TIM3_ctr_clk = 72 MHz)
TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
TIM_TimeBaseStructure.TIM_Period = 65535;                                // ARR set to max val
TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure); 

NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;				  // Interrupt fuer Timer 4 einrichten (Set up interrupt for timer 4)
NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
NVIC_Init(&NVIC_InitStructure);

TIM_EncoderInterfaceConfig(TIM4,TIM_EncoderMode_TI12, TIM_ICPolarity_Rising, TIM_ICPolarity_Falling);     // The timer encoder is prepared using the TIM_EncoderInterfaceConfig (...) function. TI12:timer counts with edges on TI1 and TI2.
TIM_Cmd(TIM4,ENABLE);

TIM_ClearITPendingBit(TIM4, TIM_IT_Update);						// TIM4 Update Interrupt darf nicht gleich zuschlagen (TIM4 update interrupt must not strike immediately)
TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);						// Ueberlauf Interrupt fuer 32 bit Wegmessung freigeben (Enable overflow interrupt for 32 bit distance measurement)

//**************************************************************************************************************************************************************


}

