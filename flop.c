#include <stdint.h>					// benutzt C99 Integer Typen

volatile int16_t tictac = 0;		// Systemzeituhr ist signed

//************************************************************************************************
//
// Universelle Zeitverwaltung mit Systick, TestFlop meldet eine abgelaufene Zeit mit TRUE zuruek (Universal time management with Systick, TestFlop reports an elapsed time with TRUE)

void SetFlop(int16_t* timer, int16_t length)
{
  *timer = (int16_t) (tictac + length);                // current tick tack value + required value
}

int TestFlop(int16_t* timer)
{
 if ( (int16_t)  (tictac - *timer) >= 0) return (1); 
 else return (0);
}

